<?php
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

Route::post('/login', [AuthController::class, 'login']);
Route::post('/sign-up',  [AuthController::class, 'singUp']);
Route::post('/confirm',  [AuthController::class, 'confirm']);
Route::group(['middleware' => 'auth.jwt'], function () {
    Route::post('/logout',  [AuthController::class, 'logout']);
    Route::post('/refresh',  [AuthController::class, 'refresh']);
    Route::post('/me',  [AuthController::class, 'me']);
});

