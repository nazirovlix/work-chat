<?php

use App\Http\Controllers\ChatController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth.jwt'], function () {
    Route::post('/send-message', [ChatController::class, 'sendMessage']);
    Route::get('/message', [ChatController::class, 'getMessages']);
    Route::get('/users', [ChatController::class, 'getUsers']);
});