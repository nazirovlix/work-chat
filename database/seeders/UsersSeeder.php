<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserInfo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'username' => 'jack',
                'phone' => '9989701234567',
                'password' => Hash::make('12345')
            ],
            [
                'username' => 'david',
                'phone' => '9989707654321',
                'password' => Hash::make('12345')
            ],
            [
                'username' => 'sarah',
                'phone' => '9989707651234',
                'password' => Hash::make('12345')
            ],
        ];

        $userInfos = [
            [
                'user_id' => 1,
                'full_name' => 'Jack D.D',
                'email' => 'jack@gmail.com',
                'projects' => '"prodom"'
            ],
            [
                'user_id' => 2,
                'full_name' => 'David A.A',
                'email' => 'david@gmail.com',
                'projects' => '"prodom"'
            ],
            [
                'user_id' => 3,
                'full_name' => 'Sarah A.A',
                'email' => 'sarah@gmail.com',
                'projects' => '"prodom"'
            ]
        ];
        User::query()->insert($users);
        UserInfo::query()->insert($userInfos);
    }
}
