<?php


namespace App\Services;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class Sms
{
    /**
     * @param $phone
     * @param $message
     * @return string
     */
    public static function send($phone, $message)
    {
        $username = env('SMS_USERNAME');
        $password = env('SMS_PASSWORD');

        $phone = clearPhone($phone);
        if (strlen($phone) == 9) {
            $phone = '998' . $phone;
        }

        $client = new Client([
            'base_uri' => 'http://91.204.239.44/broker-api/send',
        ]);
        $messages = [
            [
                'recipient' => $phone,
                'message-id' => time() . mt_rand(),
                'sms' => [
                    'originator' => 4800,
                    'content' => [
                        'text' => $message
                    ]
                ]
            ]
        ];
        $request = $client->post('', [
            'auth' => [$username, $password],
            'json' => [
                'messages' => $messages
            ]
        ]);
        $stream = $request->getBody();
        $stream->rewind();
        if ($request->getStatusCode() == 200) {
            return true;//$request->getBody()->getContents();
        }
        return false;
    }

    /**
     * @param $phone
     * @param $code
     * @return bool|string
     */
    public static function sendMessageToSingUpUser($phone)
    {
        $code = self::getCode($phone);
        $message = 'Код подтверждения для нового пользователя: ' . $code;
        return self::send($phone, $message);
    }

    /**
     * @param $phone
     * @return int
     */
    public static function getCode($phone)
    {
        $code = mt_rand(100000, 999999);
        Cache::forget($phone);
        Cache::add($phone, $code, 1400);
        return $code;
    }

    /**
     * @param $code
     * @param string $session_key
     * @return bool
     */
    public static function checkEqual($code, $session_key = 'code')
    {
        return Cache::get($session_key) == $code;
    }

    public static function forgotCode($session_key = 'code')
    {
        return Cache::forget($session_key);
    }


}