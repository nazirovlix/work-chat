<?php

namespace App\Events;

use App\Models\Message;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class NewMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $receiver_id = $this->message->sender_id === $this->message->chatroom->sender_id ? $this->message->chatroom->receiver_id : $this->message->chatroom->sender_id;
        return ['messages.' . $receiver_id];
    }

    /**
     * @return string
     */
    public function broadcastAs()
    {
        return 'messages';
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
        return [
            'id' => $this->message->id,
            'chatroom_id' => $this->message->chatroom_id,
            'message' => $this->message->message,
            'file' => $this->message->file ? asset(Storage::url($this->message->file)) : null,
            'created_at' => $this->message->created_at,
            'is_read' => false,
            'is_yours' => false,
            'sender_name' => optional($this->message->sender->userInfo)->full_name,
            'sender_id' => $this->message->sender_id
        ];
    }

}
