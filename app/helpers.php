<?php

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Collection;

if (!function_exists('success_out')) {

    /**
     * @param Collection|LengthAwarePaginator|array|Authenticatable|null|string|boolean $data
     * @param null|bool|array $links
     * @param null|array $errors
     * @return ResponseFactory|\Illuminate\Http\Response
     */
    function success_out($data, $links = null, $errors = null)
    {
        if ($links) {
            $links = [
                'count' => $data->count(),
                'current_page' => $data->currentPage(),
                'from' => $data->firstItem(),
                'to' => $data->lastItem(),
                'last_page' => $data->lastPage(),
                'per_page' => $data->perPage(),
                'total' => $data->total(),
            ];
            $data = $data->getCollection();
        }
        return response([
            'success' => true,
            'data' => $data,
            'links' => $links,
            'errors' => $errors
        ]);
    }
}

if (!function_exists('error_out')) {

    /**
     * @param $errors
     * @param int $code
     * @param null $message
     * @return ResponseFactory|\Illuminate\Http\Response
     */
    function error_out($errors, $code = 422, $message = null)
    {
        return response([
            'success' => false,
            'data' => null,
            'message' => $message,
            'errors' => $errors
        ], $code);
    }
}

if (!function_exists('success_ads_out')) {

    /**
     * @param $data
     * @param $tops
     * @return ResponseFactory|\Illuminate\Http\Response
     */
    function success_ads_out($data, $tops)
    {
        $links = [
            'count' => $data->count(),
            'current_page' => $data->currentPage(),
            'from' => $data->firstItem(),
            'to' => $data->lastItem(),
            'last_page' => $data->lastPage(),
            'per_page' => $data->perPage(),
            'total' => $data->total(),
        ];
        $data = $data->getCollection();

        $data = [
            "ads" => $data,
            "tops" => $tops
        ];
        return response([
            'success' => true,
            'data' => $data,
            'links' => $links,
            'errors' => null
        ]);
    }
}

if (!function_exists('cyrl_to_latin')) {

    /**
     * @param $text
     * @return string
     */
    function cyrl_to_latin($text)
    {
        $cyrl = ['Й', 'Ц', 'У', 'К', 'Е', 'Н', 'Г', 'Ш', 'Щ', 'З', 'Х', 'Ъ', 'Ф', 'Ы', 'В', 'А', 'П', 'Р', 'О', 'Л', 'Д', 'Ж', 'Э', 'Я', 'Ч', 'С', 'М', 'И', 'Т', 'Ь', 'Б', 'Ю', 'Ё',
            'й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ', 'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э', 'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю', 'ё',
            'Ғ', 'Қ', 'Ў', 'Ҳ', 'ғ', 'қ', 'ў', 'ҳ', '‘'
        ];

        $lat = ['Y', 'S', 'U', 'K', 'E', 'N', 'G', 'SH', 'SH', 'Z', 'X', "'", 'F', 'I', 'V', 'A', 'P', 'R', 'O', 'L', 'D', 'J', 'E', 'YA', 'CH', 'S', 'M', 'I', 'T', "'", 'B', 'YU', 'YO',
            'y', 's', 'u', 'k', 'e', 'n', 'g', 'sh', 'sh', 'z', 'x', "'", 'f', 'i', 'v', 'a', 'p', 'r', 'o', 'l', 'd', 'j', 'e', 'ya', 'ch', 's', 'm', 'i', 't', "'", 'b', 'yu', 'yo',
            "G`", 'Q', "O`", 'H', 'g`', 'q', 'o`', 'h', "'"
        ];

        return str_replace($cyrl, $lat, $text);
    }
}

if (!function_exists('make_slug')) {

    /**
     * @param $text
     * @return mixed
     */
    function make_slug($text)
    {
        return str_replace(' ', '-', cyrl_to_latin($text));
    }

}

if (!function_exists('clearPhone')) {

    /**
     * @param $phone
     * @return mixed
     */
    function clearPhone($phone)
    {
        $phone = str_replace(" ", "", $phone);
        $phone = str_replace("-", "", $phone);
        $phone = str_replace("+", "", $phone);

        return $phone;
    }
}

if (!function_exists('clearChars')) {

    /**
     * @param $str
     * @return string|string[]|null
     */
    function clearChars($str)
    {
        return preg_replace('/[^A-Za-z0-9]/', '', $str);
    }
}

if (!function_exists('clearTile')) {

    /**
     * @param $str
     * @return string|string[]|null
     */
    function clearTile($str)
    {
        return str_replace(["\r\n", "\r", "\n"], '', trim($str));
    }
}

if (!function_exists('clearPriceUSD')) {

    /**
     * @param $priceUSD
     * @return string|string[]|null
     */
    function clearPriceUSD($priceUSD)
    {
        $isConventionalUnit = preg_match('/у\.е\./', $priceUSD);
        $price = null;
        if ($isConventionalUnit === 1) {
            $price = substr($priceUSD, 0, strpos($priceUSD, ' у.е.'));
        }
        $price = str_replace(' ', '', $price);
        return (int)$price;
    }
}

if (!function_exists('clearPrice')) {

    /**
     * @param $priceUSD
     * @return string|string[]|null
     */
    function clearPrice($priceUSD)
    {
        $isConventionalUnit = preg_match('/у\. е\./', $priceUSD);
        $price = null;
        if ($isConventionalUnit === 1) {
            $price = substr($priceUSD, 0, strpos($priceUSD, ' у. е.'));
        }
        $price = str_replace(' ', '', $price);
        return (int)$price;
    }
}

if (!function_exists('formatExpiryDate')) {

    /** For CARDS 12/20 => 12.20
     * @param $date
     * @return string
     */
    function formatExpiryDate($date)
    {
        $items = explode("/", $date);

        if (count($items) == 2)
            return $items[1] . $items[0];
        return $date;
    }
}

if (!function_exists('cache_remember')) {

    /** For CARDS 12/20 => 12.20
     * @param $date
     * @return string
     */
    function cache_remember($key, $callback, $time = 600)
    {
        return \Cache::remember($key, $time, function () use ($callback) {
            return $callback;
        });
    }
}

if (!function_exists('generate_webp_image')) {
    function generate_webp_image($file, $compression_quality = 80)
    {
        // check if file exists
        if (!file_exists($file)) {
            return false;
        }

        // If output file already exists return path
        $output_file = $file . '.webp';
        if (file_exists($output_file)) {
            return $output_file;
        }

        $file_type = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        if (function_exists('imagewebp')) {

            switch ($file_type) {
                case 'jpeg':
                case 'jpg':
                    $image = imagecreatefromjpeg($file);
                    break;

                case 'png':
                    $image = imagecreatefrompng($file);
                    imagepalettetotruecolor($image);
                    imagealphablending($image, true);
                    imagesavealpha($image, true);
                    break;

                case 'gif':
                    $image = imagecreatefromgif($file);
                    break;
                default:
                    return false;
            }
            $image = imagecreatetruecolor($image, 400, 400);
            // Save the image
            $result = imagewebp($image, $output_file, $compression_quality);
            if (false === $result) {
                return false;
            }

            // Free up memory
            imagedestroy($image);

            return $output_file;
        } elseif (class_exists('Imagick')) {
            $image = new Imagick();
            $image->readImage($file);

            if ($file_type === 'png') {
                $image->setImageFormat('webp');
                $image->setImageCompressionQuality($compression_quality);
                $image->setOption('webp:lossless', 'true');
            }

            $image->writeImage($output_file);
            return $output_file;
        }

        return false;
    }
}

if (!function_exists('resize_image')) {
    function resize_image($file, $w, $h)
    {
        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($w / $h > $r) {
            $newwidth = $h * $r;
            $newheight = $h;
        } else {
            $newheight = $w / $r;
            $newwidth = $w;
        }
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        $output_file = $file . 'aa.jpg';
        $result = imagewebp($dst, $output_file, 80);
        imagedestroy($dst);

        return $dst;
    }
}

if (!function_exists('checkPhone')) {
    function checkPhone($phone)
    {
        $phone = clearPhone($phone);
        if (strlen($phone) > 9) {
            $phone = substr($phone, 3, 9);
        }
        return $phone;
    }
}
if (!function_exists('app_version')) {

    /**
     * @return array|string|null
     */
    function app_version()
    {
        return request()->header('appVersion') ?? null;
    }
}

if (!function_exists('type_os')) {

    /**
     * @return array|string|null
     */
    function type_os()
    {
        return request()->header('typeOS') ?? null;
    }
}

if (!function_exists('clear_html_tags')) {

    /**
     * @param $text
     * @return string
     */
    function clear_html_tags($text)
    {
        $text = str_replace("</p>", " \n", $text);
        $text = str_replace("<br />", "", $text);
        $text = str_replace("\r\n", "\n", $text);
        return strip_tags($text);
    }
}

if (!function_exists('convert_utf')) {


    /**
     * @param $text
     * @return array|bool|false|string|string[]|void|null
     */
    function convert_utf($text)
    {
        if (is_string($text)) {
            return mb_convert_encoding(trim($text), 'UTF-8', 'HTML-ENTITIES');
        } elseif (is_array($text)) {
            $ret = [];
            foreach ($text as $i => $v) $ret[$i] = convert_utf($v);
            return implode(',', $ret);
        } elseif (is_object($text)) {
            foreach ($text as $i => $v) $text->$i = convert_utf($v);
            return $text;
        }
        return $text;
    }

    if (!function_exists('isEmail')) {
        function isEmail($email): bool
        {
            $data['username'] = $email;
            $validation = Validator::make($data, ['username' => 'string|email']);
            if (!$validation->fails()) {
                return true;
            }
            return false;
        }
    }
}

if (!function_exists('n_tag')) {

    /**
     * @param $text
     * @return string
     */
    function n_tag($text)
    {
        return $text . "\n ";
    }
}
if (!function_exists('is_json')) {
    function is_json($string, $return_data = false)
    {
        try {
            $data = json_decode($string);
            return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
        } catch (Exception $e) {
            return false;
        }
    }
}

