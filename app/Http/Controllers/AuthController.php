<?php


namespace App\Http\Controllers;


use App\Models\User;
use App\Services\Sms;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function singUp(Request $request)
    {
        $data = $request->validate([
            'username' => 'required|string|unique:users,username',
            'phone' => 'required|string|unique:users,phone',
            'password' => 'required|min:4',
            'password_confirmation' => 'required|required_with:password|same:password',
        ]);
        if (Sms::sendMessageToSingUpUser($data['phone']))
            return success_out(['message' => 'Код подтверждения отправлен']);
        return error_out([], 422, 'Ошибка');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function confirm(Request $request)
    {
        $data = $request->validate([
            'code' => 'required|integer',
            'username' => 'required|string|unique:users,username',
            'phone' => 'required|string|unique:users,phone',
            'password' => 'required|min:4',
            'password_confirmation' => 'required|required_with:password|same:password',
        ]);
        if (Sms::checkEqual($data['code'], $data['phone'])) {
            $user = new User();
            $user->fill($data);
            if ($user->save()) {
                $user->userInfo()->create();
                Sms::forgotCode();
                $token = auth('api')->login($user);
                return $this->respondWithToken($token);
            }
            return error_out([], 422, 'Ошибка');
        }
        return error_out(['code' => 'Неправильный код'], 422);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $data = $request->validate([
            'username' => 'required|string|max:255',
            'password' => 'required|string'
        ]);

        $user = User::query()
            ->where('username', $data['username'])
            ->orWhere('phone', $data['username'])
            ->first();
        if (!$user || !Hash::check($data['password'], $user->password))
            return error_out(['username' => 'Неверный логин или пароль'], 422);
        $token = auth('api')->login($user);
        return $this->respondWithToken($token);
    }

    /**
     * @return ResponseFactory|Response
     */
    public function me()
    {
        $user = Auth::user();
        $user->userInfo;
        return success_out($user);
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return ResponseFactory|Response
     */
    public function logout()
    {
        \auth('api')->logout();

        return success_out(['message' => 'Вы Успешно вышли']);
    }

    /**
     * Refresh a token.
     *
     * @return ResponseFactory|Response
     */
    public function refresh()
    {
        return $this->respondWithToken(\auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return ResponseFactory|Response
     */
    protected function respondWithToken($token)
    {
        return success_out([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'success' => true,
        ]);
    }
}