<?php


namespace App\Http\Controllers;


use App\Http\Resources\MessageResource;
use App\Http\Resources\UsersResource;
use App\Models\Chatroom;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{

    public $userId;

    public function __construct()
    {
        $this->userId = Auth::id();
    }

    /**
     * @param Chatroom $chatroom
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function sendMessage(Chatroom $chatroom, Request $request)
    {
        $data = $request->validate([
            'receiver_id' => 'required|integer|exists:users,id',
            'message' => 'nullable|string|max:2048',
            'file' => 'nullable|file|max:5120'
        ]);
        if ($request->hasFile('file'))
            $data['file'] = $chatroom->uploadFile($request, 'file');

        $room = $chatroom->firstOrCreateChatroom($this->userId, $data['receiver_id']);
        if ($message = $room->createMessage($this->userId, $data))
            return success_out(new MessageResource($message));

        return error_out([], '422', 'Ошибка при отправке сообщение');
    }

    /**
     * @param Chatroom $chatroom
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getMessages(Chatroom $chatroom, Request $request)
    {
        $data = $request->validate([
            'receiver_id' => 'required|integer|exists:users,id',
        ]);

        $room = $chatroom->firstOrCreateChatroom($this->userId, $data['receiver_id']);
        $messages = Message::query()
            ->where('chatroom_id', $room->id)
            ->orderByDesc('created_at')
            ->paginate();

        return success_out(MessageResource::collection($messages), true);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getUsers()
    {
        $users = User::query()
            ->where('id','<>', Auth::id())
            ->with('userInfo', 'chatroomSender', 'chantroomReceiver')
            ->get();
        return success_out(UsersResource::collection($users));
    }

}