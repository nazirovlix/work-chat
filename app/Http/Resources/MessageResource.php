<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'chatroom_id' => $this->chatroom_id,
            'message' => $this->message,
            'file' => $this->file ? asset(Storage::url($this->file)) : null,
            'created_at' => $this->created_at,
            'is_yours' => Auth::id() == $this->sender_id,
        ];
    }
}
