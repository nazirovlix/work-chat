<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class UsersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $push_notification = true;
        $lastMessage = null;
        if ($this->chatroomSender) {
            $lastMessage = optional($this->chatroomSender->lastMessage)->message;
            $push_notification = $this->chatroomSender->sender_push_notification;
        } elseif ($this->chantroomReceiver) {
            $lastMessage = optional($this->chantroomReceiver->lastMessage)->message;
            $push_notification = $this->chantroomReceiver->receiver_push_notification;
        }

        return [
            'id' => $this->id,
            'username' => $this->username,
            'phone' => $this->phone,
            'full_name' => optional($this->userInfo)->full_name,
            'avatar' => optional($this->userInfo)->avatar ? asset(Storage::url(optional($this->userInfo)->avatar)) : null,
            'email' => optional($this->userInfo)->email,
            'push_notification' => $push_notification,
            'last_message' => $lastMessage
        ];
    }
}
