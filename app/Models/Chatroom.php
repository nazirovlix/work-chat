<?php

namespace App\Models;

use App\Events\NewMessage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * @property integer $id
 * @property integer $sender_id
 * @property integer $receiver_id
 * @property boolean $sender_push_notification
 * @property boolean $receiver_push_notification
 * @property boolean $is_closed
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property User $sender
 * @property User $receiver
 * @property Message[] $messages
 */
class Chatroom extends BaseModel
{
    use SoftDeletes;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['sender_id', 'receiver_id', 'sender_push_notification', 'receiver_push_notification', 'is_closed', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo('App\Models\User', 'sender_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receiver()
    {
        return $this->belongsTo('App\Models\User', 'receiver_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }

    public function lastMessage()
    {
        return $this->hasOne('App\Models\Message')->orderByDesc('created_at');
    }

    /**
     * @param $sender_id
     * @param $receiver_id
     * @return \Illuminate\Database\Eloquent\Builder|Model|object|null
     */
    public function firstOrCreateChatroom($sender_id, $receiver_id)
    {
        $chatroom = self::query()
            ->where(function ($q) use ($sender_id, $receiver_id) {
                return $q->where(function ($qy) use ($sender_id, $receiver_id) {
                    return $qy->where('sender_id', $sender_id)
                        ->where('receiver_id', $receiver_id);
                })->orWhere(function ($qy) use ($sender_id, $receiver_id) {
                    return $qy->where('sender_id', $receiver_id)
                        ->where('receiver_id', $sender_id);
                });
            })
            ->where('is_closed', false)
            ->first();
        if (!$chatroom)
            $chatroom = Chatroom::query()->create([
                'sender_id' => $sender_id,
                'receiver_id' => $receiver_id
            ]);
        return $chatroom;
    }


    public function createMessage($sender_id, $data)
    {
        $message = Message::query()->create([
            'chatroom_id' => $this->id,
            'sender_id' => $sender_id,
            'message' => $data['message'] ?? null,
            'file' => $data['file'] ?? null
        ]);
        try {
            event(new NewMessage($message));
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
        return $message;
    }
}
