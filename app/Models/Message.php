<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $chatroom_id
 * @property integer $sender_id
 * @property string $message
 * @property string $file
 * @property boolean $is_read
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Chatroom $chatroom
 * @property User $sender
 */
class Message extends Model
{
    use SoftDeletes;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['chatroom_id', 'sender_id', 'message', 'file', 'is_read', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function chatroom()
    {
        return $this->belongsTo('App\Models\Chatroom');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo('App\Models\User', 'sender_id');
    }
}
