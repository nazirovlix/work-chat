<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * @mixin Builder
 * @method static \App\Models\BaseModel query()
 * @method BaseModel filterWhere(...$condition)
 * @method BaseModel orFilterWhere(...$condition)
 */
class BaseModel extends Model
{
    const STATUS_ACTIVE = 10;
    const STATUS_INACTIVE = 0;

    /**
     * @param Builder $query
     * @param mixed ...$args
     * @return Builder
     */
    public function scopeFilterWhere(Builder $query, ...$args)
    {
        if (count($args) === 1) {
            if (is_array($args[0])) {
                $args = $args[0];
            } else return $query;
        }
        if (count($args) === 2) {
            if (!is_null($args[1])) {
                return $query->where(...$args);
            }
        } else if (count($args) === 3) {
            if (!is_null($args[2])) {
                return $query->where(...$args);
            }
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param mixed ...$args
     * @return Builder
     */
    public function scopeOrFilterWhere(Builder $query, ...$args)
    {
        if (count($args) === 1) {
            if (is_array($args[0])) {
                $args = $args[0];
            } else return $query;
        }
        if (count($args) === 2) {
            if (!is_null($args[1])) {
                return $query->orWhere(...$args);
            }
        } else if (count($args) === 3) {
            if (!is_null($args[2])) {
                return $query->orWhere(...$args);
            }
        }
        return $query;
    }

    /**
     * Upload file by key
     * @param Request $request
     * @param $key
     * @return false|string|null
     */
    public function uploadFile(Request $request, $key)
    {
        if ($request->hasFile($key)) {
            $image = $request->file($key);
            return $this->upload($image);
        }

        return null;
    }

    /**
     * Upload files by key
     * @param Request $request
     * @param $key
     * @return array
     */
    public function uploadFiles(Request $request, $key)
    {
        $images = [];
        if ($request->hasFile($key)) {
            foreach ($request->file($key) as $image) {
                $images[] = $this->upload($image);
            }
        }

        return $images;
    }

    /**
     * @param UploadedFile $image
     * @return mixed
     */
    public function upload(UploadedFile $image)
    {
        $name = rand() . '-' . time() . '-' . $image->getClientOriginalName();
        return Storage::disk('public')->putFileAs('files', $image, $name);
    }
}
